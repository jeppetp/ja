var width = window.innerWidth
var height = window.innerHeight
// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))
// ...I miss python
let range = ((n) => [...Array(n).keys()])

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(30)
}

var t = 0
let colNum = 2
let sin_res = 128

function amplitude(x, y, off) {
  return sin(off + dist([x,y], [sin_res/2,colNum/2]))
}

function rings(x, y, t) {
  let rings = 13
  let rd = (height*0.7) / rings
  let speed = 100
  let magic = ((cos(t)*speed) - 60) * 2
  stroke(100, 50, 190)
  noFill()
  for (var i=1; i<rings; i++) {
    circle(x, y, rd*i + magic)
    //circle(x, y, rd*i)
  }
}

function draw() {
  background(100, 50, 60)

  fill(100, 50, 90)
  noStroke()
  let sin_offset = t
  for (i=0;i<colNum; i++){
    let y_offset = (i / colNum) * height/2
    beginShape()
    vertex(width - 1,  height-1)
    vertex(0, height-1)
    vertex(0, height/2)
    for (j=0; j<sin_res; j++){
      // rect(0, 0, 100, 100)
      let x = map(j, 0, sin_res-1, 0, width-1)
      let y = height * amplitude(j,i,y_offset)
      vertex(x, y - 175*noise((t*2) + x*1337) + 100)
      //vertex(x, y)
    }
    endShape(CLOSE)
  }

  for (var ringx=0; ringx < width + 100; ringx += 50) {
    rings(ringx, height, t + (ringx * 0.002))
  }

  t+=0.03
}
